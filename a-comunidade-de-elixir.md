# A Comunidade de Elixir

## Tema, Descrição do Tema e Integrantes da Equipe

Integrantes:

  1. Adolfo Gustavo Serra Seca Neto
  1. Karl Popper
  1. Newton da Costa


Este é o Trabalho sobre Representacão de Conhecimento...


##  Frases e Fórmulas

### Propriedades de “objetos”

| Frase   |  Elixir Outlaws é um Podcast.   |
| ----------- | ----------- |
| Fórmula   |  Podcast("Elixir Outlaws")       |
| Definições de Predicados/Funções   | Podcast(X): X é um Podcast|

(...)

### Relações entre “objetos”

| Frase   |  José Valim é o Criador de Elixir.   |
| ----------- | ----------- |
| Fórmula   |  Criador("José Valim", "Elixir")       |
| Definições de Predicados/Funções   | Criador(X,Y): X é o criador de Y.|


(...)

### Generalizações Universais

| Frase   |  Todo podcast tem ao menos um host.   |
| ----------- | ----------- |
| Fórmula   |  &forall;x (Podcast(x) &rarr; &exist;y (Host(y,x))) |
| Definições de Predicados/Funções   | Host(X,Y): X é host de Y.|

(...)

## Assinatura

&Sigma;=[R¹, R&sup2;, C, F¹, F&sup2;, V]

## Modelos

### Exemplo de modelo que satisfaz todas as fórmulas (M&#x2081;)

1. Universo de valores concretos
 
A={vc1, vc2, vc3}

2. Constantes

  - "José Valim"<sup>M&#x2081;</sup>  = vc1
  - "Elixir"<sup>M&#x2081;</sup> = vc2
  - "Elixir Outlaws"<sup>M&#x2081;</sup> = vc3 


### Exemplo de modelo que não satisfaz todas as fórmulas (M&#x2082;)




